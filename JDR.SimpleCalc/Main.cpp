
// Simple Calculator
// Justin Rankin

#include <iostream>
#include <conio.h>

using namespace std;

float Add(float, float);
float Subtract(float, float);
float Multiply(float, float);
bool Divide(float, float, float&);

int main(){



	float numb1, numb2;
	char oper;
	float answer = 0;
	char contin;


	do {

		cout << "Please enter a basic math operation : + or - or * or ! for division " "\n";
		cin >> oper;
		cout << "Please enter your first positive number:" "\n";
		cin >> numb1;
		cout << "Please enter your second positive number:" "\n";
		cin >> numb2;

		switch (oper) {
		case '+':
			cout << "Your answer is " << Add(numb1, numb2);
			break;
		case '-':
			cout << "Your answer is " << Subtract(numb1, numb2);
			break;
		case '*':
			cout << "Your answer is " << Multiply(numb1, numb2);
			break;
		case '!':
			if (Divide(numb1, numb2, answer))
			{
				cout << "Your answer is " << answer;
			}
			else
			{
				cout << "You cannot divide by 0";
			}
			break;
		}

		cout << "\n" "Would you like to perform another function? (Y/N)" "\n";
		cin >> contin;
	} while (contin == 'Y');




	(void)_getch();
	return 0;
}

float Add(float num1, float num2)
{
	float answer;
	answer = num1 + num2;
	return answer;
}

float Subtract(float num1, float num2)
{
	float answer;
	answer = num1 - num2;
	return answer;
}

float Multiply(float num1, float num2)
{
	float answer;
	answer = num1 * num2;
	return answer;
}
bool Divide(float num1, float num2, float& answer)
{
	float quotient;
	if (num2 == 0)
	{
		return false;
	}
	else
	{
		quotient = num1 / num2;
		answer = quotient;
		return true;
	}


}



